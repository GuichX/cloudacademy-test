import isWithinInterval from 'date-fns/isWithinInterval/index.js'

/**
 * Function to check if the given date is between a range
 * @param date - {string} - The date to check to
 * @param startDate - {string} - The start date of the range
 * @param endDate - {string} - The end date of the range
 * @return {boolean | *}
 */
export function isBetweenDate(date, startDate, endDate) {
  const start = new Date(startDate)
  const end = new Date(endDate)

  return isWithinInterval(new Date(date), { start, end })
}
