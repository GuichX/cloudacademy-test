/**
 * The consts used in the project
 * @type {{scoreKey: string, endDate: string, extraKey: string, slug: string, startDate: string}}
 */
export const consts = {
  slug: 'aggregation-overall',
  scoreKey: 'score',
  extraKey: 'extra',
  startDate: '2015-08-19T14:00:19.352000Z',
  endDate: '2015-10-12T07:27:47.493000Z'
}
