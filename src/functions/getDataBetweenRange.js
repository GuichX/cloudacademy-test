import { isBetweenDate } from '../utils/isBetweenDate.js'

/**
 * Function to get the data parsed between the given dates
 * @param data - {object} - The data to parse
 * @param startDate - {string} - The start date
 * @param endDate - {string} - The end date
 * @param slug - {string} - The slug where to get the data
 * @param scoreKey - {string} - The key to check for the data
 * @return {{mappedSeries: object[], endDate: string, startDate: string}}
 */
export default function getDataBetweenRange(data, startDate, endDate, slug, scoreKey) {
  const aggregationData = data.filter((obj) => obj.slug === slug)

  const aggregationDataFilteredByKey = aggregationData[0].details.filter((obj) => obj.key === scoreKey)

  const [scoreData] = aggregationDataFilteredByKey
  const { series } = scoreData

  const mappedSeries = series.filter((obj) => isBetweenDate(obj.x, startDate, endDate))

  return {
    startDate,
    endDate,
    mappedSeries
  }
}
