import mergeWith from 'lodash.mergewith'
import { isBetweenDate } from '../utils/isBetweenDate.js'

/**
 * Function to get the data parsed not within a range of dates
 * @param data - {object} - The data to parse
 * @param startDate - {string} - The start date
 * @param endDate - {string} - The end date
 * @param slug - {string} - The slug where to get the data
 * @param scoreKey - {string} - The key to check for the data
 * @param extraKey - {string} - The key to check for the data
 * @return {{mergedArray: object[], endDate: string, startDate: string}}
 */
export default function getDataNotInRange(data, startDate, endDate, slug, scoreKey, extraKey) {
  const aggregationData = data.filter((obj) => obj.slug === slug)

  const aggregationDataFilteredByScoreKey = aggregationData[0].details.filter((obj) => obj.key === scoreKey)
  const aggregationDataFilteredByExtraKey = aggregationData[0].details.filter((obj) => obj.key === extraKey)

  const [scoreData] = aggregationDataFilteredByScoreKey
  const [extraData] = aggregationDataFilteredByExtraKey
  const { series: scoreSeries } = scoreData
  const { series: extraSeries } = extraData

  const mappedScoreSeries = scoreSeries.filter((obj) => !isBetweenDate(obj.x, startDate, endDate))
  const mappedExtraSeries = extraSeries.filter((obj) => !isBetweenDate(obj.x, startDate, endDate))

  const mergedArray = mergeWith(mappedScoreSeries, mappedExtraSeries, (obj, src) => {
    if (obj.x === src.x) return { ...obj, ...src, y: { point: obj.y, ...src.y } }
  })

  return {
    startDate,
    endDate,
    mergedArray
  }
}
