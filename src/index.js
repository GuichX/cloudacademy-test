import path from 'path'
import fs from 'fs'
import { consts } from './utils/consts.js'
import getDataBetweenRange from './functions/getDataBetweenRange.js'
import getDataNotInRange from './functions/getDataNotInRange.js'

const rawData = fs.readFileSync(path.join(path.resolve(), 'data/data.json'))
const parsedData = JSON.parse(`${rawData}`)

const { data } = parsedData
const { slug, scoreKey, startDate, endDate, extraKey } = consts

const expectedData = getDataBetweenRange(data, startDate, endDate, slug, scoreKey)
const expectedDataNotInRange = getDataNotInRange(data, startDate, endDate, slug, scoreKey, extraKey)

if (process.env.writeToFile) {
  fs.writeFile('dataBetweenRange.json', JSON.stringify(expectedData), 'utf8', (err) => {
    if (err) console.info(err)
  })

  fs.writeFile('dataNotBetweenRange.json', JSON.stringify(expectedDataNotInRange), 'utf8', (err) => {
    if (err) console.info(err)
  })
} else {
  console.info(expectedData)
  console.info(expectedDataNotInRange)
}
