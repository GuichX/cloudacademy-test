import expectedData from './mocks/expectedData.json'
import data from '../data/data.json'
import { consts } from '../src/utils/consts'
import getParsedData from '../src/functions/getDataBetweenRange'

describe('CloudAcademy Test Suite', () => {
  it('Should return the data with a date in a range', () => {
    const { data: jsonData } = data
    const { slug, scoreKey, startDate, endDate } = consts

    expect(getParsedData(jsonData, startDate, endDate, slug, scoreKey).mappedSeries).toEqual(expectedData)
  })
})
