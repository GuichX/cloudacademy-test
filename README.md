# Cloud Academy Senior FE Engineering

This repo contains the code of the assignment for the Senior FE Engineering position at Cloud Academy.

## How to run

1. Clone the repo
2. Install the required packages (Yarn: `$ yarn install` Npm: `$ rm yarn.lock` then `$ npm install`)
3. Run one of the available [Scripts](#scripts) via `npm/yarn run script`

## Scripts

The available scripts are:

1. `console`: Display the output to the console
2. `file`: Will write two json with the output
3. `test`: Will run jest to test the condition of the first function given the expected result
   
